#include <stdlib.h>
#include <stdio.h>
#include "rr.h"

float* RoundRobin(int *S_Time, int *R_Time, int num){
	int count = num; /*the size of the array*/
	int process[count];
	int length[count];
	int quantum = 10;
	float* rArray= malloc(sizeof(float)*2);
	int waitT = 0; /*avrage wait time*/
	int turnA = 0; /*avrage turn arround time*/
	int clockT = 1;
	int processNum = 0; /*what process number im working on*/
	int i = 0; /*in case i need to store a number for a line or two*/
	int finished = 0; /*the number of jobs ive finished*/
	int jobTurn = 0; /*the turn arround time for the current job*/
	int jobWait = 0; /*the wait time for the current job*/


	while(i<count){
		process[i] = S_Time[i];
		i++;
	}
	i = 0;
	while(i<count){
		length[i] = R_Time[i];
		i++;
	}
	i=0;
	i = process[0];
	i = 0;
	while(finished <= count){
	if (length[processNum] <= quantum){
		//printf("the current clock time is %d\nthe process number %d\n", clockT,processNum);
		clockT = clockT + R_Time[processNum];
		jobTurn = clockT - S_Time[processNum];
		//printf("the jobs turn arround time is %d\n",jobTurn);
		turnA = turnA + jobTurn;
		//printf("the running total is for turn arround time %d\n",turnA);
		jobWait = jobTurn - R_Time[processNum];
		//printf("the jobs wait time is %d\n",jobWait);
		waitT = waitT + jobWait;
		//printf("the running total is for wait time %d\n",waitT);
		length[processNum] = 0;
		finished = finished + 1;
		if(finished == count){
			break;
		}
		processNum = processNum +1;
		if(processNum >= count){
			processNum = 0;
		}

		jobTurn = 0;
		jobWait = 0;

	}else if(length[processNum] > quantum){
		//printf("the current job is %d\n",processNum);
		length[processNum] = length[processNum] - quantum;
		processNum++;
		if(processNum >= count){
			processNum = 0;
		}

		clockT = clockT + quantum;
		//printf("the current clock time is %d\n",clockT);
	}
	while(length[processNum] == 0){
		if(processNum >= count){
			processNum = 0;
			break;
		}

		processNum++;
	}
		if(finished == count){
			break;
		}
	
	}

	float avgWait = waitT;
//		printf("\n wait time is %f\n", avgWait);
	float avgTurn = turnA;
//		printf("\n turn arround time is %f\n",avgTurn);
	avgWait = (avgWait/count);
	avgTurn = (avgTurn/count);
	
	rArray[0]=avgWait;
	rArray[1]=avgTurn;
	return rArray;
}

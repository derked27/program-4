/*Jason Hamner
  CS 3060-002, Spring 2014
  Assngment 4*/

/*Promise of Originality
  I promise that this source code file has, in it's entirety, been
  written by myself and by no other person or persons. If at any time an
  exact copy of this source code is found to be used by another person in
  this term, I understand that both myself and the student that submitted
  the copy will receive a zero on this assignment.*/


/* Shortest Job First */

/* Jason Hamner - ke7ori@gmail.com*/
float* sjf_func(int * S_Time, int * B_Time, int cout);

int find_lowest(int * S_Time, int * B_Time, int clock);



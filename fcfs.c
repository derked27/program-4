/*David Koch
  CS 3060-002, Spring 2014
  Assngment 4*/

/*Promise of Originality
  I promise that this source code file has, in it's entirety, been
  written by myself and by no other person or persons. If at any time an
  exact copy of this source code is found to be used by another person in
  this term, I understand that both myself and the student that submitted
  the copy will receive a zero on this assignment.*/

#include<stdio.h>/*Provides print*/
#include<stdlib.h>/*Provides exit*/
#include"fcfs.h"/*Includes header file*/

float *fcfs(int *s, int *b, int size)
{
	int clock = 0;
	int position = 0;
	int avgWait = 0;
	int avgTurn = 0;
	int jobWait = 0;
	int jobTurn = 0;
	int i = 0;
	int start[size];
	int burst[size];
	float *avgTimes = malloc(sizeof(float)*2);

	while(i < size)
	{
	  start[i] = s[i];
	  burst[i] = b[i];
	  i++;
	}
	i = 0;
	
	while(position < size)
	{
	  clock += burst[position];	
	  jobTurn = clock - start[position];
	  jobWait = jobTurn - burst[position];
	  position ++;
	  avgWait += jobWait;
	  jobWait = 0;
	  avgTurn += jobTurn;
	  jobTurn = 0;
	}

	float wait = avgWait;
	wait /= size;
	avgTimes[0] = wait;
	float turn = avgTurn;
	turn /= size;
	avgTimes[1] = turn;
	
  return avgTimes;
}

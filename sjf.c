/*Jason Hamner
  CS 3060-002, Spring 2014
  Assngment 4*/

/*Promise of Originality
  I promise that this source code file has, in it's entirety, been
  written by myself and by no other person or persons. If at any time an
  exact copy of this source code is found to be used by another person in
  this term, I understand that both myself and the student that submitted
  the copy will receive a zero on this assignment.*/

#include<stdio.h>
#include<stdlib.h>
#include"sjf.h"

//Find Lowest Run Time Upto Clock in ms
// Returns -1 if not found
int find_lowest(int * S_Time, int * B_Time, int clock)
{
    int idx = 0;
    int lowest_job = -1;
    int lowest_brust = -1;

    //for(idx = 0; idx <= clock; idx++)
    while(S_Time[idx] <= clock)
    {
        if(((B_Time[idx] < lowest_brust) || (lowest_brust == -1)) && B_Time[idx] != 0)
        {
                lowest_job = idx;
                lowest_brust = B_Time[idx];
        }
        idx++;
    }

    return lowest_job;
}

float* sjf_func(int * S_Time, int * B_Time, int count)
{
    int job_num[count];
    int brust_num[count];
    int idx = 0;
    int current_job = -1;
    float *thingie = malloc(sizeof(float) *2);
    int clock = 0;
    int t_wait = 0;
    int t_turn_around = 0;

    for(idx = 0; idx < count; idx++)
    {
        job_num[idx] = S_Time[idx];
        brust_num[idx] = B_Time[idx];
    }

    //idx = 0;

    do
    {
        current_job = find_lowest(job_num, brust_num, clock);

        if(current_job != -1)
        {
            clock += brust_num[current_job];
            brust_num[current_job] = 0;

            //Stats
            //t_turn_around += S_Time[current_job] - clock;
            t_turn_around += clock - job_num[current_job];
            //t_wait += t_turn_around - B_Time[current_job];
            t_wait += (clock - job_num[current_job]) - B_Time[current_job];
        }
        else
        {
            clock += 1;
        }

        //idx++;

    }while(((current_job != -1) || (clock < 5 )));


    thingie[0] = (float)t_wait / count;
    thingie[1] = (float)t_turn_around / count;

    return thingie;

}

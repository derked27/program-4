/*Jason Hamner
  CS 3060-002, Spring 2014
  Assngment 4*/

/*Promise of Originality
  I promise that this source code file has, in it's entirety, been
  written by myself and by no other person or persons. If at any time an
  exact copy of this source code is found to be used by another person in
  this term, I understand that both myself and the student that submitted
  the copy will receive a zero on this assignment.*/

#include<stdio.h>
#include<stdlib.h>
#include"fcfs.h"
#include"sjf.h"
#include"srtn.h"
#include"rr.h"

#define MAX_ARRAY_SIZE (100)

int readFromFile(FILE *fp, int *array1, int *array2)
{

  int count = 0;
  while(count < MAX_ARRAY_SIZE && fscanf(fp, "%d", &array1[count]) == 1 && fscanf(fp, "%d", &array2[count]) == 1)
  {
    count++;
  }
  return count;
}

int main()
{
  int sizeOfArray;
  float *array;
  int subNumber[MAX_ARRAY_SIZE];
  int burstNumber[MAX_ARRAY_SIZE];
  float avgWaitTime;
  float avgTurnAroundTime;

  sizeOfArray = readFromFile(stdin, subNumber, burstNumber);
  printf("I read %d processes from the file\n", sizeOfArray);

  array = fcfs(subNumber, burstNumber, sizeOfArray);
  avgWaitTime = array[0];
  avgTurnAroundTime = array[1];
  printf("First Come First Serves average wait time is: %.2f and average turn around time is: %.2f\n", avgWaitTime, avgTurnAroundTime);
  free (array);

  array = sjf_func(subNumber, burstNumber, sizeOfArray);
  avgWaitTime = array[0];
  avgTurnAroundTime = array[1];
  printf("Shortest Job Firsts average wait time is: %.2f and average turn around time is: %.2f\n", avgWaitTime, avgTurnAroundTime);
  free (array);

  array = srtn(subNumber, burstNumber, sizeOfArray);
  avgWaitTime = array[0];
  avgTurnAroundTime = array[1];
  printf("Shortest Job Nexts average wait time is: %.2f and average turn around time is: %.2f\n", avgWaitTime, avgTurnAroundTime);
  free (array);

  array = RoundRobin(subNumber, burstNumber, sizeOfArray);
  avgWaitTime = array[0];
  avgTurnAroundTime = array[1];
  printf("Round Robin average wait time is: %.2f and average turn around time is: %.2f\n", avgWaitTime, avgTurnAroundTime);
  free (array);

  return 0;
}

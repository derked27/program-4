

#include "srtn.h"
#include <stdio.h>
#include <stdlib.h>
int FindLowest(int *timeLeft, int position) {
  int min;
  int i;
  if(position == 0) {
    return 0;
  }
  for(i = 0; i < position; i++) {
    if(i==0) {
      min = timeLeft[i];
    }
    else if(timeLeft[i] < min) {
      min = timeLeft[i];
    }

  }
  if(min <= 0) {
    return -1;
}
return min;
}


float* srtn(int *arrivalTime, int *burstTime, int count) {
  int current = 0;
  int previous = 1;
  int timeLeft[count];
  int totalWait[count];
  int turnAround[count];
  int arrivalT[count];
  int burstT[count];
  int time = 0;
  int av = 0;
  int ta = 0;
  float average, turnA;
  float * arrayToPass= malloc(sizeof(float)*2);
  int overlapTime = 0;
  int i;

 for(i = 0; i < count; i++){
 arrivalT[i] = arrivalTime[i];
 burstT[i] = burstTime[i];
} 
  for(i = 0; i < count; i++) {
    overlapTime = 0;
    timeLeft[i] = burstT[i];
    timeLeft[current] =timeLeft[current] - (arrivalT[current] -  previous);
   // printf("time left %d\n", timeLeft[current]);
      if(timeLeft[current] < 0) {
        overlapTime = 0 - timeLeft[current];
	totalWait[current] = time - overlapTime;
        turnAround[current] = totalWait[current] - burstT[current];
      }
    previous = arrivalT[i] - overlapTime;
    current =  FindLowest(timeLeft, i);
    }
  time = arrivalT[count];
  while(current != -1) {
    current = FindLowest(timeLeft, count);
    time +=  timeLeft[current];
    totalWait[current] = time - arrivalT[current];
    turnAround[current] = totalWait[current] - burstT[current];
    timeLeft[current] = 0;
  }
  for(i = 0; i < count; i++) {
    av += totalWait[i];
    ta += turnAround[i];
    i++;
  }
  average = av / count;
  turnA = ta / count;
  arrayToPass[0] = average;
  arrayToPass[1] = turnA;

  return arrayToPass;
}


